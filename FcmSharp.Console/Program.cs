﻿// Copyright (c) Philipp Wagner. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using FcmSharp.Requests;
using FcmSharp.Settings;

namespace FcmSharp.ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Sistema Messaggistica Emergenze Poste Italiane - E.P.I.");
            Console.WriteLine();
           
            string edificio = "posteitaliane";

            Console.WriteLine("Inserisci Titolo Messaggio.");
            string titolo = Console.ReadLine();

            Console.WriteLine("Inserisci Testo Messaggio.");
            string messaggio = Console.ReadLine();



            // Read the Credentials from a File, which is not under Version Control:
            var settings = FileBasedFcmClientSettings.CreateFromFile(@"C:\chiave_fcm\poste-italiane-a7e24-firebase-adminsdk-2zy4h-3798c29c7e.json");


            IDictionary<string, string> MESSAGGIO_DATA = new Dictionary<string, string>();

            MESSAGGIO_DATA.Add("title", titolo);
            MESSAGGIO_DATA.Add("body", messaggio);
            MESSAGGIO_DATA.Add("canale", edificio);

            // Construct the Client:
            using (var client = new FcmClient(settings))
            {
                //var notification = new Notification
                //{
                //    Title = "Allarme",
                //    Body = "Corona Virus Impazza Per L'edificio"
                //};


                // The Message should be sent to the News Topic:
                var message = new FcmMessage()
                {
                    ValidateOnly = false,

                    Message = new Message
                    {
                        Topic = edificio,
                        //Notification = notification,
                        Data = MESSAGGIO_DATA

                    }
                };

                // Finally send the Message and wait for the Result:
                CancellationTokenSource cts = new CancellationTokenSource();

                // Send the Message and wait synchronously:
                var result = client.SendAsync(message, cts.Token).GetAwaiter().GetResult();

                // Print the Result to the Console:
                Console.WriteLine("Data Message ID = {0}", result.Name);


            }
        }
    }
}
